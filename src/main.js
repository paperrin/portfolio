import '@/assets/css/reset.css';
import '@/assets/css/global.css';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/validations.js';
import {
	FontAwesomeIcon,
	FontAwesomeLayers,
} from "@fortawesome/vue-fontawesome";
import '@/fontAwesome.js';

Vue.config.productionTip = false;

function stringPrefixWithSeparator(str, prefix, separator = '') {
	return [prefix, str].filter(Boolean).join(separator);
}

function objectKeysPrefixWithSeparator(obj, prefix, separator) {
	const prefixed = {};

	Object.entries(obj).forEach(([key, value]) => {
		const prefixedKey = stringPrefixWithSeparator(key, prefix, separator);
		prefixed[prefixedKey] = value;
	});
	return prefixed;
}

Vue.component('FontAwesomeIcon', FontAwesomeIcon);
Vue.component('FontAwesomeLayers', FontAwesomeLayers);

Vue.mixin({
	methods: {
		$(...args) {
			const hasBlock = typeof args[0] !== 'object';
			let block, modifiers;

			if (hasBlock)
				[ block, modifiers ] = args;
			else
				[ modifiers ] = args;

			const prefixedClassName = stringPrefixWithSeparator(block, this.$options.name, '_');

			if (typeof modifiers === 'object') {
				const prefixedModifiers = objectKeysPrefixWithSeparator(modifiers, prefixedClassName, '-');

				return [
					prefixedClassName,
					prefixedModifiers,
				];
			}
			return prefixedClassName;
		}
	}
});

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
