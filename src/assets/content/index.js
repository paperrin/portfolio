export default {
	landing: {
		title: "I'm <em>Paul</em> Perrin",
		sub: "a <b>software developer</b> from Paris.",
		incentive: "<em>scroll down</em> to learn more"
	},
	bio: {
		title: "Hi, I'm <em>Paul</em>. How's it going?",
		short: "Because programming is all about <em>continous learning</em>, I'm looking for a team that allows me to perpetually get outside of my comfort zone and which will always be ready to improve what's already established."
	},
	projects: {
		title: "My Projects",
		itemTemplate: {
			overview: "Overview",
			links: { live: "Visit", source: `Code` },
			role: "My Role",
			difficulties: "Difficulties",
			solutions: "My Solutions",
			features: "Features"
		},
		items: [
			{
				title: "Personal Portfolio",
				img: {
					desktop: "content/img/portfolio/home-desktop.png",
					phone: "content/img/portfolio/home-phone.png",
					tablet: "content/img/portfolio/home-tablet.png",
				},
				overview: `My personal portfolio, currently in development.`,
				links: {
					live: "https://paulperr.in",
					source: "https://gitlab.com/paperrin/portfolio"
				},
				techs: [
					"JS",
					"Vue",
					"VueX",
					"SASS",
					"Inline-SVG",
					"Google Cloud Functions",
					"Secret Manager",
					"Express",
					"Express-Validator",
					"Node-Mailer",
				]
			},
			{
				title: "SpaceX Launches",
				img: {
					desktop: "content/img/spacex-launches/home-desktop.png",
					phone: "content/img/spacex-launches/home-phone.png",
					tablet: "content/img/spacex-launches/home-tablet.png",
				},
				overview: "Share your enthusiasm for SpaceX's next launch, or view their past launches ( did you really miss them? ).",
				links: {
					live: "https://spacex.paulperr.in",
					source: "https://gitlab.com/paperrin/spacex-launches"
				},
				techs: [
					"JS",
					"React",
					"Redux",
					"Redux Thunk",
					"Styled-Components",
					"CSS",
					"Axios",
					"Luxon",
					"Cypress",
					"Enzyme",
					"Faker",
				]
			},
			{
				title: "SW:RA Search System",
				img: {
					phone: "content/img/swra-search-system/home-phone.png",
					tablet: "content/img/swra-search-system/home-tablet.png",
				},
				overview: `Personal practice project including a web app and REST backend able to search through
					<a href='https://swapi.dev' target='_blank'>SWAPI.dev</a>'s data (Star Wars REST api).`,
				links: {
					source: "https://gitlab.com/paperrin/swra-search-system"
				},
				techs: [
					"JS",
					"React",
					"React-Router",
					"Redux",
					"Font-Awesome",
					"Styled-Components",
					"CSS",
					"Hapi",
					"Axios",
					"Express",
				]
			},
			{
				title: "Bar à Crêpes",
				img: {
					desktop: "content/img/bar-a-crepes/home-desktop.png",
					phone: "content/img/bar-a-crepes/home-phone.png",
					tablet: "content/img/bar-a-crepes/home-tablet.png",
				},
				overview: "Personal exercise to try out Vue.JS made in about 2 days of work. A list of products can be viewed by category, and added to a shopping cart.",
				links: {
					live: "https://baracrepes.paulperr.in",
					source: "https://gitlab.com/paperrin/bar-a-crepes"
				},
				features: [
					"Show product list by category",
					"Product detail view",
					"Add x ammount of products to basket",
					"Modify and view order items and quantities from basket",
				],
				techs: [
					"JS",
					"Vue",
					"VueX",
					"CSS",
				]
			},
			{
				title: "ZoonStudio @ Kwalia",
				img: {
					desktop: "content/img/zoon-studio/lumiere-desktop.png",
					phone: "content/img/zoon-studio/lumiere-phone.png",
					tablet: "content/img/zoon-studio/lumiere-tablet.png",
				},
				overview: `ZoonStudio is a new way for authors to create interactive and animated content:
					digital comics, animated storyboards... <br />No code required!`,
				links: {
					live: "https://kwalia.fr",
				},
				techs: [
					"JS",
					"React",
					"React-Native",
					"Redux",
					"Electron",
					"Pixi.JS",
					"Web Workers",
					"CSS-Modules",
				].sort()
			},
			{
				title: "Ray Tracer",
				img: {
					desktop: "content/img/rt/corr04.png"
				},
				overview: "With this software, you can describe 3D scenes to be rendered in real-time via a custom scripting language I invented.",
				links: {
					source: "https://github.com/paperrin/raytracer",
				},
				techs: [
					"C",
					"GLFW",
					"OpenGL",
					"OpenCL",
					"Makefile",
					"CMake",
				]
			}
		]
	}
};
