import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faSquare,
} from '@fortawesome/free-solid-svg-icons';
import {
	faGitlab,
	faGithub,
	faLinkedinIn,
	faTwitter,
	faAngellist,
} from '@fortawesome/free-brands-svg-icons';

export default {
	SQUARE: ['fas', 'square'],
	GITLAB: ['fab', 'gitlab'],
	GITHUB: ['fab', 'github'],
	LINKEDIN: ['fab', 'linkedin-in'],
	TWITTER: ['fab', 'twitter'],
	ANGELLIST: ['fab', 'angellist'],
};

library.add(
	faSquare,
	faGitlab,
	faGithub,
	faLinkedinIn,
	faTwitter,
	faAngellist,
);