export { default as MyInput } from './MyInput.vue';
export { default as MyTextArea } from './MyTextArea.vue';
export { default as MyCheckbox } from './MyCheckbox.vue';
export { default as MyButton } from './MyButton.vue';