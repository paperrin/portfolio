import { extend } from 'vee-validate';
import { required, min, max } from 'vee-validate/dist/rules';

extend('required', {
	...required,
	message: 'This field is required',
});

extend('min', {
	...min,
	message: 'Must be at least {length} characters long',
});

extend('max', {
	...max,
	message: 'Must be at most {length} characters long',
});

extend('email', {
	validate: value => value.match(/^.+@.+$/),
	message: 'Must be a valid mail address',
})