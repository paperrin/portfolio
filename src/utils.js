export function getUniqueId() {
	return `ID-${++getUniqueId.lastUID}`;
}
getUniqueId.lastUID = -1;