const themeNames = Object.freeze({
	DARK: 'dark',
	LIGHT: 'light',
});

export default themeNames;