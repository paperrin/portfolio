const getters = Object.freeze({
	GET_THEME_NAME: 'GET_THEME_NAME',
});

export default getters;