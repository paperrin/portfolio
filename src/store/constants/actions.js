const actions = Object.freeze({
	SET_THEME_NAME: 'SET_THEME_NAME',
});

export default actions;