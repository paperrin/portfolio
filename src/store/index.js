import Vue from 'vue'
import Vuex from 'vuex'
import content from "@/assets/content/index.js";
import {
	mutations,
	getters,
	actions,
} from './constants';

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		content,
		themeName: 'dark',
	},
	getters: {
		[getters.GET_THEME_NAME]: state => state.themeName,
	},
	mutations: {
		[mutations.SET_THEME_NAME](state, themeName) {
			state.themeName = themeName;
		}
	},
	actions: {
		[actions.SET_THEME_NAME]({ commit }, themeName) {
			commit(mutations.SET_THEME_NAME, themeName);
		}
	},
});