import { mount } from "@vue/test-utils";
import ContactMe from "@/components/ContactMe.vue";

describe('ContactMe.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(ContactMe);
	});

	afterEach(() => {
		wrapper.destroy();
	});

	it('renders all required inputs', () => {
		expect(wrapper.find('.ContactMe_Input[name="name"]').exists()).toBe(true);
		expect(wrapper.find('.ContactMe_Input[name="mail"]').exists()).toBe(true);
		expect(wrapper.find('.ContactMe_Input[name="text"]').exists()).toBe(true);
	});
});